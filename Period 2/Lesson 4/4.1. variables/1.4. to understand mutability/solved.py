my_list = [1, 2, 205, 16.436, 2j]
my_dictionary = {"key1": 10, "key2": "salam dünya", "key3": 16.3}
my_set = {44, 12, 20.6, 40j}

# dəyişənlərin dəyərlərini, tiplərini və adreslərini (id() funksiyasının köməkliyi ilə) çap edirik
print("type of my_list variable is:", type(my_list))
print("value of my_list variable is:", my_list)
print("address of my_list variable is:", id(my_list))

print("type of my_dictionary variable is:", type(my_dictionary))
print("value of my_dictionary variable is:", my_dictionary)
print("address of my_dictionary variable is:", id(my_dictionary))

print("type of my_set variable is:", type(my_set))
print("value of my_set variable is:", my_set)
print("address of my_set variable is:", id(my_set))


# dəyişənin dəyərini dəyişərək onun adresinin dəyişmədiyini yoxlayırıq
print("old address of my_list variable was:", id(my_list))
my_list[2] = "salam"
print("new address of my_integer variable is:", id(my_list))
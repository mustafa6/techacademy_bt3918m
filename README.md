# TechAcademy Dərsləri

Tech Academy Azərbaycanda data elmi, süni intellekt və robotloşma üzrə ixtisaslaşmış kodlaşdırma düşərgəsidir. Qlobal bazarda Python proqramlaşdırma dilinin önəminin sürətlə artdığını nəzərə alaraq xarici mütəxəssislərlə rəqabət aparacaq səviyyədə yerli kadrlar yetişdirmək Techacademy-nin əsas məqsədidir. Python proqramlaşdırma dilinin spesifikliyini nəzərə alaraq kollektivimiz hər zaman tələbələrimizə karyera məsləhətlərinin verilməsi, lazımi iş yerlərinə yönləndirilməsi istiqamətində yardımçı olmağa hazırdır.

Tədris prosesinin yüksək standartlara cavab verməsi üçün Techacademy olaraq tələbələrimizi ən son avadanlıq, geniş və işıqlı otaq və dərs üçün lazım olan digər bütün ləvazımatlarla təmin edirik. Professional müəllim kollektivimizin tədris metodikasında yenilikçi olması və hər bir tələbəyə fərdi yanaşması qısa müddət ərzində tələbələrin Python proqramlaşdırma dilini sərbəst formada tətbiq etməsinə şərait yaradır. Siz də proqramlaşdırma sahəsi üzrə uğurlu karyeranıza sağlam addımlarla başlamaq istəyirsinizsə Techacademy-ə müraciət edin!

[TechAcademy.az](https://techacademy.az)

## Təlimçilərimiz

* **[Elşad Ağayev](https://techacademy.az)** *Təlimçi, TechAcademy Bakı, Azərbaycan* - Amerikanın Silikon Vadisində yerləşən Obitx və Gigasoft şirkətlərində fəaliyyət göstərmiş, 10 illik development təcrübəli, Berkeley Universitetinin sertifikatını almış mütəxəssisdir

* **[Şəhriyar Rzayev](https://techacademy.az)** *Təlimçi, TechAcademy Bakı, Azərbaycan* - Amerikanın Silikon Vadisində yerləşən Percona LLC şirkətində fəaliyyət göstərmiş, 5 illik Development və QA təcrübəli, Azərbaycanda Python və MySQL istifadəçilərinin qrup lideridir

## Mentorlarımız

* **[Qərib Mehtiyev](https://techacademy.az)** *Co-Founder & CTO, WeTravel San Fransico, California, USA* - 11 illik development, Dev-Ops və İT-idarəetmə təcrübəli mütəxəssisdir. Amerikanın Silikon Vadisində bir çox şirkətrləin təsisçisidir. 2009-cu ildə Amerikaya köçərək ilk şirkətini təsis etmişdir

* **[Teymur Sadıqov](https://techacademy.az)** *Elmi Tədqiqatçı, NASA Washington DC, USA* - Teymur Sadıqov dünyanın aerokosmik tədqiqatlar mərkəzi hesab olunan NASA-da elmi tədqiqatla məşğul olan ilk və yeganə azərbaycanlıdır

* **[Emin Şərifov](https://techacademy.az)** *Texniki Proqram Meneceri, Expedia İnc San Francisco, California, USA* - Emin Sadıqov Amerikanın Silikon Vadisində yerləşən WeTravel, Travana, AccountingSuite və s. kimi məşhur şirkətlərdə fəaliyyət göstərmiş təcrübəli mütəxəssisdir

# ISTIFADEDCHI adlı dəyişən yaradın və whoami komandasının köməkliyi ilə istifadəçinin adını bu dəyişənə mənimsədin
ISTIFADEDCHI=$(whoami)

# OS adlı dəyişən yaradın və bu dəyişənə uname komandasının vasitəsi ilə əməliyyat sisteminin növü və kernelin versiyasını mənimsədin
OS=$(uname -s)"-"$(uname -r)

# ISTIFADEDCHI və OS dəyişənlərinin birləşməsindən əmələ gələn sözləri DIRNAME dəyişəninə mənimsədin, istifadəçinin home folderində eyni adlı direktoriya yaradın
DIRNAME=${ISTIFADEDCHI}"-"${OS}
mkdir ~/$DIRNAME

# daxil olduğunuz istifadəçi adından run olan bütün proseslərin siyahısını hər hansı bir fayla yazın və onu $DIRNAME folderinə saxlayın
ps aux | grep $(whoami) > ~/$DIRNAME/$(whoami)_ps.txt

# $DIRNAME folderində 10 ədəd direktoriya yaradın və hər birinin içərisində 100 ədəd .txt fayl yaradın
mkdir ~/$DIRNAME/subdirs_{1..10}
touch ~/$DIRNAME/subdirs_{1..10}/file_{1..100}.txt

# find ilə $DIRNAME folderindəki sonu 2.txt ilə bitən bütün faylların siyahısını a-dan z-yə sıralanmış şəkildə fayla yazın
find ~/$DIRNAME -type f -name "*2.txt" | sort > ~/$DIRNAME/sorted_list.txt

# $DIRNAME folderindəki əvvəli file_ başlayan, sonu .txt ilə bitən faylları və onları daşıyan folderləri silin
rm -rf ~/$DIRNAME/subdirs_{1..10}
